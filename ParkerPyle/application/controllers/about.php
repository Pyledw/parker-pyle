<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
         * This is the default function for the controller.  It
	 * 
         * @author David Pyle <pyledw@gmail.com>
	 */
	public function index()
	{
            
            //loading helper
            $this->load->helper('url');
            
            
                
                //Setting the title for the page if no title is set
                $title = "About";
                $data['title'] = $title. " - Parker & Pyle";
                
                //Leading all views
                $this->load->view('templates/header.php',$data);//Header to initialize the html - Also contains navigation bar.
                $this->load->view('about.php',$data);//Sidebar Data
                $this->load->view('templates/leftArea.php');//Main content that will be loaded pased on page content
                $this->load->view('templates/footer.php');//Footer containing footer information
		
                
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */